#include <SPI.h>
#include <SD.h>

File myFile;
int cant_mapas = 0;
bool cardInitialized = false;

void printDirectory(File dir, int numTabs) {
  cant_mapas = 0;
  dir.rewindDirectory();
  
  while (true) {

    File entry =  dir.openNextFile();
    if (! entry) {
      // no more files
      break;
    }
    for (uint8_t i = 0; i < numTabs; i++) {
      Serial.print('\t');
    }
    Serial.print(entry.name());
    if (entry.isDirectory()) {
      Serial.println("/");
      //printDirectory(entry, numTabs + 1);
    } else {
      // files have sizes, directories do not
      Serial.print("\t\t");
      Serial.println(entry.size(), DEC);
      cant_mapas++;
    }
    entry.close();
  }
}

bool listarNiveles() {
  if (!cardInitialized) {
    if (!SD.begin(53)) {
        Serial.print("Initializing SD card...");
        Serial.println("initialization failed!");
      return false;
    }
    Serial.println("initialization done."); 
    cardInitialized = true;
  }

  myFile = SD.open("/");

  printDirectory(myFile, 0);

  Serial.print("La Tarjeta de Memoria contiene ");
  Serial.print(cant_mapas);
  Serial.println(" Niveles!");

  return cant_mapas != 0;
}

char *cargarNivel(int level, int &alto, int &ancho)
{
  char *mapData;
  alto = 0;
  ancho = 0;
  Serial.print("Initializing SD card...");

  /*
  if (!SD.begin(53))
  {
    Serial.println("initialization failed!");
    while (1)
      ;
  }
  Serial.println("initialization done.");
  */
  
  char fileName[] = "level_0.txt";
  fileName[6] = level + '0';

  // open the file. note that only one file can be open at a time,
  // so you have to close this one before opening another.
  myFile = SD.open(fileName);
  if (myFile)
  {
    Serial.print("Loading ");
    Serial.print(fileName);
    Serial.println("...");

    // Leer Alto del mapa
    if (myFile.available())
    {
      char c = myFile.read();
      while (myFile.available() && c != '\r')
      {
        alto *= 10;
        alto += (c - '0');
        c = myFile.read();
      }
      if (c == '\r')
      {
        // Skipear el \n
        c = myFile.read();
      }
      //alto++;
      if (myFile.available())
      {
        c = myFile.read();
        // Leer Ancho del mapa
        while (myFile.available() && c != '\r')
        {
          ancho *= 10;
          ancho += (c - '0');
          c = myFile.read();
        }
        if (c == '\r')
        {
          // Skipear el \n
          c = myFile.read();
        }
        // Crear arreglo del mapa
        mapData = malloc(alto * ancho * sizeof(char));
        int i = 0;
        // Leer el mapa
        while (myFile.available())
        {
          c = myFile.read();
          if (c != '\r' && c != '\n')
          {
            mapData[i] = c - '0';
            //Serial.write(mapData[i]);
            //if ((i + 1) % ancho == 0)
            //{
            //Serial.println();
            //}
            i++;
          }
        }
      }
    }

    // close the file:
    myFile.close();
  }
  else
  {
    // if the file didn't open, print an error:
    Serial.print("error opening ");
    Serial.println(fileName);
  }
  return mapData;
}

