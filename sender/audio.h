/*
  Melody

  Plays a melody

  circuit:
  - 8 ohm speaker on digital pin 8

  created 21 Jan 2010
  modified 30 Aug 2011
  by Tom Igoe

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Tone
*/

#define GND_AUDIO 9
#define TONE_AUDIO 8
#include "effect.h"
#include "notes.h"
#include "snd_tetris.h"

//27.77777777 milisegundos

//variables de musica
int note_step = 0;
int size_melody;
int thisNote = 0;
int noteDuration;
int noteDuration_millis;


//variables de efectos
byte effect_flag = 0;
int index_effect;
int effect_note_step = 0;
int size_effect;
int effect_note = 0;
int noteEffectDuration;
int noteEffectDuration_millis;

//flags de MODO
byte music_on_flag = 0;
byte effects_on_flag = 0;

float constante_tiempo = 32.25f / 2;

bool flag_rebote = true;

void rebote_off(){
  flag_rebote = false;
}

void rebote_on(){
  flag_rebote = true;
  
}

void setup_music(){
      //configurando timer
    //243
  pinMode(GND_AUDIO,OUTPUT);
  digitalWrite(GND_AUDIO,LOW);  
  cli(); // stop interrupts
  TCNT4  = 0;
  TCCR4A = 0;     // set entire TCCR3A register to 0
  TCCR4B = 0;     // same for TCCR3B

  // set compare match register to desired timer count: 800 Hz
  OCR4A = 216;//interrumpe cada 27,777777 milisegundos antes era 433
  // turn on CTC mode:
  TCCR4B |= (1 << WGM12);

  // Set CS10 and CS12 bits for 1024 prescaler:
  TCCR4B |= (1 << CS40) | (1 << CS42);
  // enable timer compare interrupt:
  TIMSK4 |= (1 << OCIE4A);
  // enable global interrupts:
  sei(); 
  

 
}

void musicON(){
  size_melody = sizeof(melody) / sizeof(int);
  music_on_flag = 1;
  effects_on_flag = 0;
  note_step = 0;
  thisNote = 0;
  Serial.println("Music ON");
}

void musicOFF(){
  music_on_flag = 0;
  effects_on_flag = 1;

}


void playMusic() {
  //Serial.println("play Music");
   //debe configurar la duracion de la nota y la cantidad de tics del timer
   noteDuration = 1000/(noteDurations[thisNote] * constante_tiempo);//tics del timer
   noteDuration_millis = 1000 / noteDurations[thisNote];//duracion en millis
   tone(TONE_AUDIO, melody[thisNote], noteDuration_millis);
}

void stopNote(){
  noTone(TONE_AUDIO);
  note_step=0;
}


void playEffectSalto(){
  
  if(effects_on_flag == 1) 
    effect_flag = 1;
  index_effect = efc_salto;
  effect_note = 0;
  size_effect = sizeof(effect_salto) / sizeof(int);
 
}

void playEffectRebote(){
  if(flag_rebote){
    if(effects_on_flag == 1) 
      effect_flag = 1;
    index_effect = efc_rebote;
    effect_note = 0;
  
    size_effect = sizeof(effect_rebote) / sizeof(int);
  }
  
}

void playEffectMuerte(){
  rebote_off();
  if(effects_on_flag == 1) 
    effect_flag = 1;
  index_effect = efc_muerte;
effect_note = 0;

  size_effect = sizeof(effect_muerte) / sizeof(int);
  
}

void playEffectAnillo(){
  rebote_off();
  if(effects_on_flag == 1) 
    effect_flag = 1;
  index_effect = efc_anillo;
effect_note = 0;

  size_effect = sizeof(effect_anillo) / sizeof(int);
  
}

void playEffectFin(){
  if(effects_on_flag == 1) 
    effect_flag = 1;
  index_effect = efc_fin;
  size_effect = sizeof(effect_fin) / sizeof(int);
  effect_note = 0;

}

void playEffectWin(){
  if(effects_on_flag == 1) 
    effect_flag = 1;
  index_effect = efc_win;
  size_effect = sizeof(effect_win) / sizeof(int);
  effect_note = 0;

}


void playEffect(){
  
//debe configurar la duracion de la nota y la cantidad de tics del timer
   noteEffectDuration = 1000/(array_effect_duration[index_effect][effect_note] * constante_tiempo);//tics del timer
   noteEffectDuration_millis = 1000 / array_effect_duration[index_effect][effect_note];//duracion en millis
   tone(TONE_AUDIO, array_effect[index_effect][effect_note], noteEffectDuration_millis);
   
}


ISR(TIMER4_COMPA_vect){
  //tengo que agregar un ciclo mas para el silencio
  if(music_on_flag == 1){
    

    if(note_step == 0){
      playMusic();
      thisNote = (thisNote+1)%size_melody;
      
      //Serial.println(noteDuration);
    }
  
    note_step++;
    
    if(note_step>noteDuration){
      //un silencio de 1 tiempo y se frena la nota
      stopNote();
    }
  }
  
  if(effects_on_flag == 1)
    if(effect_flag == 1){
      //inicializo un efecto
      if(effect_note_step == 0){
        playEffect();
        effect_note = (effect_note+1);
      //Serial.println(noteDuration);
      }
      
      effect_note_step++;
      
     if(effect_note_step>noteEffectDuration){
      //un silencio de 1 tiempo y se frena la nota
       // stopMusic();
       effect_note_step=0;
      }
  
      if(effect_note>= size_effect){//fin del efecto
        rebote_on();
        effect_flag = 0;
        effect_note_step = 0;
        effect_note = 0;
      }
    }
  
  
}
