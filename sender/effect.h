#define efc_salto 0;
#define efc_rebote 1;
#define efc_muerte 2;
#define efc_anillo 3;
#define efc_fin 4;
#define efc_win 5;

#include "notes.h"

int effect_salto[] ={
  //NOTE_AS4, 0
  NOTE_DS4, NOTE_C4, NOTE_G3, NOTE_CS3, NOTE_CS2, 
  NOTE_CS1, NOTE_CS2, NOTE_CS3, NOTE_G3, NOTE_C4
};

int effect_salto_duration[] = {
  //2, 8
  32, 32, 32, 32, 32,
  32, 32, 32, 32, 32,
};

int effect_rebote[] ={
  NOTE_CS5, NOTE_DS6, 0, NOTE_CS5, NOTE_DS6 
  };
  
int effect_rebote_duration[] = {
  32, 32, 32, 32, 32
};

int effect_muerte[] ={
  NOTE_F4, NOTE_E5, NOTE_DS4, NOTE_D5, 
  NOTE_GS3, NOTE_A4, NOTE_AS3, NOTE_B4
  };

int effect_muerte_duration[] = {
  12, 12, 12, 12,
  12, 12, 12, 12,
};

int effect_anillo[] ={
  NOTE_E6, NOTE_G6, NOTE_E7, NOTE_C7, NOTE_D7, NOTE_G7,
};

int effect_anillo_duration[] = {
  16,16,16,16,16,16,
};

int effect_fin[] ={
  NOTE_DS5, NOTE_D5, NOTE_CS5, NOTE_C5, NOTE_D5, NOTE_CS5, NOTE_C5, NOTE_B4, 
  NOTE_CS5, NOTE_C5, NOTE_B4, NOTE_AS4, NOTE_A4, 
  NOTE_B4, NOTE_AS4, NOTE_A4, NOTE_GS4, 
  NOTE_FS4, NOTE_F4, NOTE_E4
  };

int effect_fin_duration[] = {
  12, 12, 12, 12, 12, 12, 12, 12, 12, 12,
  12, 12, 12, 12, 12,
  12, 12, 12, 12,
  12, 12, 12,
};

int effect_win[] ={
  523, 391, 329, 391, 659, 523, 391, 523, 
  783, 659, 523, 659, 1046, 
  783, 659, 783, 1318, 
  };

int effect_win_duration[] = {
  8, 8, 8, 8, 8, 8, 8, 8, 8, 8,
  8, 8, 8, 8, 8,
  8, 8, 8, 8,
};

int* array_effect[] ={
	effect_salto,
	effect_rebote,
	effect_muerte,
	effect_anillo,
	effect_fin,
  effect_win
};

int* array_effect_duration[] ={
	effect_salto_duration,
	effect_rebote_duration,
	effect_muerte_duration,
	effect_anillo_duration,
	effect_fin_duration,
  effect_win_duration
};
