//#define anchoMapa 103
//#define altoMapa 7

#define UP 0
#define DOWN 1
#define LEFT 2
#define RIGHT 3

float vel[4] = {0,0,0,0};

bool saltando = false;

float max_vel = 8;
float min_vel = 1.2f; // Tiene que ser mayor que gravedad
float gravity = 1.0f;

float acel[4] = {0, gravity, 0, 0};

int flag_colision[4] = {0,0,0,0};

float coef_rest = 0.6f;

int x_relativo = 0;

int j =0;

int x_pelota = 4;
int y_pelota = 15;

int muerte = 0;

int colision_up(){
  if(y_pelota<-2) return 1;

  if(x_pelota % 2 == 0)
  {
    int coord = ((y_pelota+8)/8 - 1)* anchoMapa + x_pelota/2;
    
    return mapa[coord];
  }
  
  else
  {
    int coord = ((y_pelota+8)/8 - 1)* anchoMapa + x_pelota/2;
    
    if(mapa[coord] ==1)
      return 1;
    else
      return mapa[coord + 1];
  }//**/
}

int colision_down(){
  if(y_pelota<0) return 0;

  if(x_pelota % 2 == 0)
  {
    int coord = (y_pelota/8 + 1)* anchoMapa + x_pelota/2;
    
    return mapa[coord];
  }
  
  else
  {
    int coord = (y_pelota/8 + 1)* anchoMapa + x_pelota/2;
    
    if(mapa[coord] == 1)
      return 1;
    else
      return mapa[coord + 1];
  }
  /**/
}


int colision_left(){

  if(x_pelota%2 == 0)
    //if(y_pelota % 8 == 0)
    {

      int coord = (y_pelota/8)* anchoMapa + x_pelota/2 - 1;
      return mapa[coord];
    }
    /*
    else
    {
      int coord = (y_pelota/8 + 1)* anchoMapa + x_pelota/2 - 1 + j;
      
      if(mapa[coord] != 0)
        return mapa[coord];
      else
        return mapa[coord + anchoMapa];
    }/**/
  else
    return 0;
}

int colision_right(){
  //if(y_pelota % 8 == 0)
  {
    int coord = (y_pelota/8)* anchoMapa + x_pelota/2 + 1;
    
    return mapa[coord];
  }
  /*
  else
  {
    int coord = (y_pelota/8 + 1)* anchoMapa + x_pelota/2 + 1 + j;
    
    if(mapa[coord] != 0)
      return mapa[coord];
    else
      return mapa[coord + anchoMapa];
  }
  /**/
}

int colision(int x_pelota, int y_pelota){

  return mapa[(y_pelota/8)* anchoMapa + x_pelota/2];
}


void fisicaInput(){


  if(up_button == 1)
      acel[UP] = 0.4f;
  if(up_button == 0)
      acel[UP] = 0;
  if(down_button == 1)
    acel[DOWN] += 0.4f;
  if(down_button == 1)
    acel[DOWN] = gravity;
  if(left_button == 1)
    vel[LEFT] = 1;
  if(left_button == 0)
    vel[LEFT] = 0;
  if(right_button == 1)
    vel[RIGHT] = 1;
  if(right_button == 0)
    vel[RIGHT] = 0;


  if(A_button == 1 && colision_down())
  {
    playEffectSalto();//EFECTO SALTO
    vel[DOWN] = -max_vel;
  }
}

void move(){

  fisicaInput();
 
      
  //UP-DOWN BEGIN
 // if(colision_up() != 1)
  y_pelota += vel[DOWN];

  //checkeo colisiones
  if(colision_up() == 1)
  {
    //printf("colision UP\n");
    if(flag_colision[UP] == 0)
    {
      //if(colision(x_pelota,y_pelota+1)==1 || colision(x_pelota+1,y_pelota+1) == 1)
      {
        vel[DOWN] = -vel[DOWN]*coef_rest;
        y_pelota += 8 - y_pelota%8;
        playEffectRebote();//EFECTO REBOTE
      }
    }
    flag_colision[UP] = 1;
  }
  else{
    flag_colision[UP] = 0;
  }
  if(colision_down()== 1){

    if(flag_colision[DOWN] == 0){

      if(vel[DOWN] > min_vel){
        vel[DOWN] = -vel[DOWN] * coef_rest;
        playEffectRebote();//EFECTO REBOTE
      }
      else
        vel[DOWN] = 0;

    }
    flag_colision[DOWN] = 1;
  }
  else{
    flag_colision[DOWN] = 0;
  }

  {
    //manejes de la velocidad y las colisiones
    vel[DOWN] += (acel[DOWN] - acel[UP]);

    if(vel[DOWN] != (float)((int) vel[DOWN]) && vel[DOWN] < 0.7f && vel[DOWN] > -0.7f){
      y_pelota ++;
    }       
    //para que deje de rebotar
    if(flag_colision[DOWN] == 1 && vel[DOWN] >= -min_vel && vel[DOWN] <= min_vel)
      vel[DOWN] = 0;

    if(flag_colision[DOWN] == 1){
      y_pelota = y_pelota - y_pelota%8;
    }

    if(y_pelota == 48){
      y_pelota = 40;
      vel[DOWN] = -vel[DOWN] * coef_rest;
      Serial.println("ENTRA");
    }

  //se corrige la altura de la pelota para que sea menor o igual a 0
   if(y_pelota<0){
    y_pelota = 0;
   }
      

    if(vel[DOWN] > max_vel)
      vel[DOWN] = max_vel;
    if(vel[DOWN] < -max_vel)
      vel[DOWN] = -max_vel;
  
    if(colision_down()!= 1)
      flag_colision[DOWN] = 0;
    if(colision_up() != 1)
      flag_colision[UP] = 0;

    //UP-DOWN END   
    //acel[UP] = 0;

  }

  //XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
  
  if(colision_left() !=1 )
    x_pelota -= vel[LEFT];
  else if(colision_left() == 1){
   // x_pelota += vel[LEFT];
    if(vel[LEFT]>0)
      playEffectRebote();//EFECTO REBOTE
  }

  if(colision_right() !=1 ){
    x_pelota += vel[RIGHT];
    flag_colision[RIGHT] = 1;
  }
  else if(colision_right() == 1)
  {
    //if((x_pelota/2) % 2 == 1 )
   // x_pelota-=vel[RIGHT];
    if(vel[RIGHT]>0)
      playEffectRebote();//EFECTO REBOTE
  }

  //aca acomodo el corrimiento inicial j
  x_relativo = x_pelota/2 - j;
  
  if(vel[RIGHT]>0 && x_pelota%2==0){
    if(x_relativo > 8)
    j = j + vel[RIGHT];
    
  }
  if(vel[LEFT]>0 && x_pelota%2==0){
    if(x_relativo < 4)
      j = j - vel[LEFT];
      
  }
 
  if(j<0)
    j = 0;

  //checkea cuando se termina el mapa
  if (j + 15 > anchoMapa) {
    j = anchoMapa - 15;
  }
  
  x_relativo = x_pelota/2 - j;
  
}
//     1 metro  = 8 pixels
//
// 0.125 metros = 1 pix
//
// 4.7 metros/seg² = 4.7/0.125 = 37.6 pix/s²
