
#include "keycodes.h"
byte up_button = 0;
byte down_button = 0;
byte left_button = 0;
byte right_button = 0;
byte A_button = 0;


void checkInput()
{
  char recv;

  while (Serial2.available())
  {
    recv = Serial2.read();

    switch (recv)
    {
    case UP_PRESS:
    {
      up_button = 1;
    }
    break;
    case UP_RELEASE:
    {
      up_button = 0;
    }
    break;
    case DOWN_PRESS:
    {
      down_button = 1;
    }
    break;
    case DOWN_RELEASE:
    {
      down_button = 0;

    }
    break;
    case LEFT_PRESS:
    {
      left_button = 1;
    }
    break;
    case LEFT_RELEASE:
    {
      left_button = 0;
    }
    break;
    case RIGHT_PRESS:
    {
      right_button = 1;
    }
    break;
    case RIGHT_RELEASE:
    {
      right_button = 0;
    }
    break;
    case BOTON_PRESS:
    {
      A_button = 1;
    }
    break;
    case BOTON_RELEASE:
    {
      A_button = 0;
    }
    break;
    }
  }
}