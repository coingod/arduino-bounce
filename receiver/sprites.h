const unsigned char img_bloque_data[ALTO_BLOQUE * ANCHO_BLOQUE] PROGMEM = {
    42, 168,
    162, 34,
    136, 138,
    162, 34,
    136, 138,
    162, 34,
    136, 138,
    42, 168
};

const unsigned char img_pinche_data[ALTO_BLOQUE * ANCHO_BLOQUE] PROGMEM = {
    3, 192,
    15, 240,
    3, 192,
    15, 240,
    3, 192,
    15, 240,
    3, 192,
    15, 240
};

const unsigned char img_anillo_data[ALTO_BLOQUE * ANCHO_BLOQUE] PROGMEM = {
    1,  64,
    5,  80,
    4,  16,
    4,  16,
    4,  16,
    4,  16,
    5,  80,
    1,  64,
};

const unsigned char img_pelota_data_0[ALTO_BLOQUE * ANCHO_BLOQUE] PROGMEM = {
    10, 160,
    42, 168,
    170, 170,
    138, 170,
    162, 170,
    136, 170,
    34,  40,
    10, 160,
};

const unsigned char img_pelota_data_1[ALTO_BLOQUE * ANCHO_BLOQUE] PROGMEM = {
    10, 160,
    34, 40,
    136, 170,
    162, 170,
    138, 170,
    170, 170,
    42,  168,
    10, 160,
};

const unsigned char img_pelota_data_2[ALTO_BLOQUE * ANCHO_BLOQUE] PROGMEM = {
    10, 160,
    40, 136,
    170, 34,
    170, 138,
    170, 162,
    170, 170,
    42,  168,
    10, 160,
};

const unsigned char img_pelota_data_3[ALTO_BLOQUE * ANCHO_BLOQUE] PROGMEM = {
    10, 160,
    42, 168,
    170, 170,
    170, 162,
    170, 138,
    170, 34,
    40,  136,
    10, 160,
};

const unsigned char img_pelota_muerta_data[ALTO_BLOQUE * ANCHO_BLOQUE] PROGMEM = {
    0, 8,
    34, 32, 
    11, 168,
    42, 186,
    174, 168,
    10, 130,
    34, 160,
    2, 2,
};

const unsigned char img_puerta_data[ALTO_BLOQUE * ANCHO_BLOQUE] PROGMEM = {
    255, 255,
    251, 187,
    238, 239,
    251, 187,
    238, 239,
    251, 187,
    238, 239,
    255, 255,
 };

 const unsigned char img_pelota_mask[ALTO_BLOQUE * ANCHO_BLOQUE] PROGMEM = {
240,  15,
192,   3,
  0,   0,
  0,   0,
  0,   0,
  0,   0,
192,   3,
240,  15,
};

const unsigned char img_pelota_muerta_mask[ALTO_BLOQUE * ANCHO_BLOQUE] PROGMEM = {
255, 243,
204, 207,
240,   3,
192,   0,
  0,   3,
240,  60,
204,  15,
252, 252,
};
