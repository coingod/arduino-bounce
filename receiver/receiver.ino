 #define ALTO_BLOQUE 8  //Lineas
#define ANCHO_BLOQUE 2 //Bytes
#define ALTO 7         //Bloques
#define ANCHO 15       //Bloques
#define TAM_CHAR 7     //img_bloque_datas
#define FILA_MATRIZ 4
#define ANCHO_EN_CHAR 30

#include <VGAX.h>
#include "sprites.h"

unsigned char bufferChar[32]; //Initialized variable to store recieved data

byte y_pelota = 25;
byte x_pelota = 16;
byte rotacion = 0;
byte pos_puerta = 0;
byte i = 0;
byte j = 0;
byte indice = 0;
int aux_index;
double t = 0;

#include "start.h"
#include "levels.h"
#include "over.h"
#include "completed.h"

void setup()
{
    VGAX::begin();
    VGAX::clear(01);
    // Begin the Serial at 9600 Baud
    Serial.begin(9600);
    //Serial.begin(19200);
    //Serial.begin(57600);

    // Mostrar Pantalla de Inicio del Juego


    // Mostrar Pantalla de Niveles del Juego
    
    
   
}

void InGame(){
  while(bufferChar[0] != 254 && bufferChar[0]!=253){
    if (Serial.available() > 0)
    {
        Serial.readBytes((char *)bufferChar, 32); //Read the serial data and store in var

        for (int i = 0; i < 32; i++)
        {
            bufferChar[i]--;
        }

        pos_puerta = bufferChar[28];
        x_pelota = bufferChar[29];
        y_pelota = bufferChar[30];
        rotacion = bufferChar[31];
        //VGAX::clear(01);
        if(bufferChar[0] != 254 && bufferChar[0] != 253)
          draw_frame();
        
    }
    VGAX::delay(50);
  }
}

void loop()
{
  PantallaInicio();
  if (PantallaNiveles()) {
    InGame();
    if(bufferChar[0] == 253){
      PantallaCompletado();
      bufferChar[0] = 1;
    }
    else{
      PantallaFin();
      bufferChar[0] = 1;
    }
   }
  VGAX::clear(00);
 
    //draw_frame();
}

void draw_frame()
{
    for (i = 0; i < ALTO; i++)
    {
        {
            for (j = 0; j < 4; j++)
            { //primero, 4 img_bloque_datas
                for (indice = 0; indice < ALTO_BLOQUE; indice++)
                {
                    aux_index = (i * ANCHO * 8 + indice * ANCHO + j) * ANCHO_BLOQUE  ;
                    if(aux_index>=0)
                    { //indice2 = 0

                        if (~bufferChar[i * FILA_MATRIZ] & (1 << (TAM_CHAR - j * 2)) && bufferChar[i * FILA_MATRIZ] & (1 << (TAM_CHAR - j * 2 - 1))) //img_bloque_dataS
                        {
                            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_bloque_data + indice * ANCHO_BLOQUE);
                        }
                        else if (bufferChar[i * FILA_MATRIZ] & (1 << (TAM_CHAR - j * 2)) && ~bufferChar[i * FILA_MATRIZ] & (1 << (TAM_CHAR - j * 2 - 1))) //PINCHES
                        {
                            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_pinche_data + indice * ANCHO_BLOQUE);
                        }
                        else if (bufferChar[i * FILA_MATRIZ] & (1 << (TAM_CHAR - j * 2)) && bufferChar[i * FILA_MATRIZ] & (1 << (TAM_CHAR - j * 2 - 1))) //ANILLOS
                        {
                            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_anillo_data + indice * ANCHO_BLOQUE);
                        }
                        else
                        {
                            vgaxfb[aux_index] = (char)0;
                        }
                    }

                    { //indice2 = 1
                        aux_index += 1;

                        if (~bufferChar[i * FILA_MATRIZ] & (1 << (TAM_CHAR - j * 2)) && bufferChar[i * FILA_MATRIZ] & (1 << (TAM_CHAR - j * 2 - 1))) //img_bloque_dataS
                        {
                            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_bloque_data + indice * ANCHO_BLOQUE + 1);
                        }
                        else if (bufferChar[i * FILA_MATRIZ] & (1 << (TAM_CHAR - j * 2)) && ~bufferChar[i * FILA_MATRIZ] & (1 << (TAM_CHAR - j * 2 - 1))) //PINCHES
                        {
                            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_pinche_data + indice * ANCHO_BLOQUE + 1);
                        }

                        else if (bufferChar[i * FILA_MATRIZ] & (1 << (TAM_CHAR - j * 2)) && bufferChar[i * FILA_MATRIZ] & (1 << (TAM_CHAR - j * 2 - 1))) //ANILLOS
                        {
                            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_anillo_data + indice * ANCHO_BLOQUE + 1);
                        }
                        else
                        {
                            vgaxfb[aux_index] = (char)0;
                        }

                    } //bucle ANCHO de img_bloque_data
                }

            } //bucle ANCHO de pantalla

            for (j = 0; j < 4; j++)
            { //segundo, 4 img_bloque_datas
                for (indice = 0; indice < ALTO_BLOQUE; indice++)
                {
                    { //indice2 = 0
                        aux_index = (i * ANCHO * 8 + indice * ANCHO + j + 4) * ANCHO_BLOQUE  ;

                        if (~bufferChar[i * FILA_MATRIZ + 1] & (1 << (TAM_CHAR - j * 2)) && bufferChar[i * FILA_MATRIZ + 1] & (1 << (TAM_CHAR - j * 2 - 1))) //img_bloque_dataS
                        {
                            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_bloque_data + indice * ANCHO_BLOQUE);
                        }
                        else if (bufferChar[i * FILA_MATRIZ + 1] & (1 << (TAM_CHAR - j * 2)) && ~bufferChar[i * FILA_MATRIZ + 1] & (1 << (TAM_CHAR - j * 2 - 1))) //PINCHES
                        {
                            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_pinche_data + indice * ANCHO_BLOQUE);
                        }
                        else if (bufferChar[i * FILA_MATRIZ + 1] & (1 << (TAM_CHAR - j * 2)) && bufferChar[i * FILA_MATRIZ + 1] & (1 << (TAM_CHAR - j * 2 - 1))) //ANILLOS
                        {
                            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_anillo_data + indice * ANCHO_BLOQUE);
                        }
                        else
                        {
                            vgaxfb[aux_index] = (char)0;
                        }
                    }

                    { //indice2 = 1
                        aux_index += 1;
                        if (~bufferChar[i * FILA_MATRIZ + 1] & (1 << (TAM_CHAR - j * 2)) && bufferChar[i * FILA_MATRIZ + 1] & (1 << (TAM_CHAR - j * 2 - 1))) //img_bloque_dataS
                        {
                            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_bloque_data + indice * ANCHO_BLOQUE + 1);
                        }
                        else if (bufferChar[i * FILA_MATRIZ + 1] & (1 << (TAM_CHAR - j * 2)) && ~bufferChar[i * FILA_MATRIZ + 1] & (1 << (TAM_CHAR - j * 2 - 1))) //PINCHES
                        {
                            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_pinche_data + indice * ANCHO_BLOQUE + 1);
                        }

                        else if (bufferChar[i * FILA_MATRIZ + 1] & (1 << (TAM_CHAR - j * 2)) && bufferChar[i * FILA_MATRIZ + 1] & (1 << (TAM_CHAR - j * 2 - 1))) //ANILLOS
                        {
                            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_anillo_data + indice * ANCHO_BLOQUE + 1);
                        }
                        else
                        {
                            vgaxfb[aux_index] = (char)0;
                        }

                    } //bucle ANCHO de img_bloque_data
                }

            } //bucle ANCHO de pantalla

            for (j = 0; j < 4; j++)
            { //tercero, 4 img_bloque_datas
                for (indice = 0; indice < ALTO_BLOQUE; indice++)
                {
                    { //indice2 = 0
                        aux_index = (i * ANCHO * 8 + indice * ANCHO + j + 8) * ANCHO_BLOQUE ;

                        if (~bufferChar[i * FILA_MATRIZ + 2] & (1 << (TAM_CHAR - j * 2)) && bufferChar[i * FILA_MATRIZ + 2] & (1 << (TAM_CHAR - j * 2 - 1))) //img_bloque_dataS
                        {
                            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_bloque_data + indice * ANCHO_BLOQUE);
                        }
                        else if (bufferChar[i * FILA_MATRIZ + 2] & (1 << (TAM_CHAR - j * 2)) && ~bufferChar[i * FILA_MATRIZ + 2] & (1 << (TAM_CHAR - j * 2 - 1))) //PINCHES
                        {
                            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_pinche_data + indice * ANCHO_BLOQUE);
                        }
                        else if (bufferChar[i * FILA_MATRIZ + 2] & (1 << (TAM_CHAR - j * 2)) && bufferChar[i * FILA_MATRIZ + 2] & (1 << (TAM_CHAR - j * 2 - 1))) //ANILLOS
                        {
                            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_anillo_data + indice * ANCHO_BLOQUE);
                        }
                        else
                        {
                            vgaxfb[aux_index] = (char)0;
                        }
                    }

                    { //indice2 = 1
                        aux_index += 1;

                        if (~bufferChar[i * FILA_MATRIZ + 2] & (1 << (TAM_CHAR - j * 2)) && bufferChar[i * FILA_MATRIZ + 2] & (1 << (TAM_CHAR - j * 2 - 1))) //img_bloque_dataS
                        {
                            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_bloque_data + indice * ANCHO_BLOQUE + 1);
                        }
                        else if (bufferChar[i * FILA_MATRIZ + 2] & (1 << (TAM_CHAR - j * 2)) && ~bufferChar[i * FILA_MATRIZ + 2] & (1 << (TAM_CHAR - j * 2 - 1))) //PINCHES
                        {
                            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_pinche_data + indice * ANCHO_BLOQUE + 1);
                        }
                        else if (bufferChar[i * FILA_MATRIZ + 2] & (1 << (TAM_CHAR - j * 2)) && bufferChar[i * FILA_MATRIZ + 2] & (1 << (TAM_CHAR - j * 2 - 1))) //ANILLOS
                        {
                            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_anillo_data + indice * ANCHO_BLOQUE + 1);
                        }
                        else
                        {
                            vgaxfb[aux_index] = (char)0;
                        }

                    } //bucle ANCHO de img_bloque_data
                }

            } //bucle ANCHO de pantalla

            for (j = 0; j < 3; j++)
            { //cuarto, los ultimos 3 img_bloque_datas
                for (indice = 0; indice < ALTO_BLOQUE; indice++)
                {
                    { //indice2 = 0
                        aux_index = (i * ANCHO * 8 + indice * ANCHO + j + 12) * ANCHO_BLOQUE ;

                        if (~bufferChar[i * FILA_MATRIZ + 3] & (1 << (TAM_CHAR - j * 2)) && bufferChar[i * FILA_MATRIZ + 3] & (1 << (TAM_CHAR - j * 2 - 1))) //img_bloque_dataS
                        {
                            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_bloque_data + indice * ANCHO_BLOQUE);
                        }

                        else if (bufferChar[i * FILA_MATRIZ + 3] & (1 << (TAM_CHAR - j * 2)) && ~bufferChar[i * FILA_MATRIZ + 3] & (1 << (TAM_CHAR - j * 2 - 1))) //PINCHES
                        {
                            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_pinche_data + indice * ANCHO_BLOQUE);
                        }

                        else if (bufferChar[i * FILA_MATRIZ + 3] & (1 << (TAM_CHAR - j * 2)) && bufferChar[i * FILA_MATRIZ + 3] & (1 << (TAM_CHAR - j * 2 - 1))) //ANILLOS
                        {
                            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_anillo_data + indice * ANCHO_BLOQUE);
                        }
                        else
                        {
                            vgaxfb[aux_index] = (char)0;
                        }
                    }

                    { //indice2 = 1
                        aux_index += 1;

                        if (~bufferChar[i * FILA_MATRIZ + 3] & (1 << (TAM_CHAR - j * 2)) && bufferChar[i * FILA_MATRIZ + 3] & (1 << (TAM_CHAR - j * 2 - 1))) //img_bloque_dataS
                        {
                            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_bloque_data + indice * ANCHO_BLOQUE + 1);
                        }

                        else if (bufferChar[i * FILA_MATRIZ + 3] & (1 << (TAM_CHAR - j * 2)) && ~bufferChar[i * FILA_MATRIZ + 3] & (1 << (TAM_CHAR - j * 2 - 1))) //PINCHES
                        {
                            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_pinche_data + indice * ANCHO_BLOQUE + 1);
                        }

                        else if (bufferChar[i * FILA_MATRIZ + 3] & (1 << (TAM_CHAR - j * 2)) && bufferChar[i * FILA_MATRIZ + 3] & (1 << (TAM_CHAR - j * 2 - 1))) //ANILLOS
                        {
                            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_anillo_data + indice * ANCHO_BLOQUE + 1);
                        }
                        else
                        {
                            vgaxfb[aux_index] = (char)0;
                        }

                    } //bucle ANCHO de img_bloque_data
                }

            } //bucle ANCHO de pantalla
              /**/

        } //fin del if principal

        //ACA TERMINA EL BUCLE PRINCIPAL

        //aca deberia intentar el corrimiento

        //CORRIMIENTO POR PIXEL
        /*if (corrimiento > 0)
        {
            for (j = 0; j < ALTO_BLOQUE; j++)
            {
                for (indice = 0; indice < ANCHO_EN_CHAR; indice++)
                {
                    aux_index = (i * 8 + j) * ANCHO_EN_CHAR + indice;

                    if (aux_index % 30 < 28)
                        if (corrimiento < 8)
                        {
                            vgaxfb[aux_index] = vgaxfb[aux_index] << corrimiento | (vgaxfb[aux_index + 1] >> (8 - corrimiento));
                        }
                        else
                        {

                            //vgaxfb[aux_index] = vgaxfb[aux_index+1];
                            vgaxfb[aux_index] = vgaxfb[aux_index + 1] << (corrimiento - 8) | (vgaxfb[aux_index + 2] >> (16 - corrimiento));
                        }

                    else
                    { //aca tengo que mirar el img_bloque_data que sigue
                        vgaxfb[aux_index] = 0;
                    }
                }
            }
        }/**/
    }

    //POSICIONANDO LA PUERTA
    if(pos_puerta > 0){
      byte x_puerta = pos_puerta>>4;
      byte y_puerta = pos_puerta&0b1111;
      for (i = 0; i < ALTO_BLOQUE; i++)
      {
        aux_index = (y_puerta * ANCHO * 8 + i * ANCHO + x_puerta) * ANCHO_BLOQUE ;;
        {
            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_puerta_data + i * ANCHO_BLOQUE);
            vgaxfb[aux_index + 1] = (byte)pgm_read_byte_near(img_puerta_data + i * ANCHO_BLOQUE + 1);
        }
        aux_index += ANCHO_BLOQUE;//dibujo a la izquierda
        if(x_puerta < 14)
        {
          vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_puerta_data + i * ANCHO_BLOQUE);
          vgaxfb[aux_index + 1] = (byte)pgm_read_byte_near(img_puerta_data + i * ANCHO_BLOQUE + 1);
        }
        
        aux_index -= (ANCHO_BLOQUE -2*ALTO_BLOQUE*ANCHO);//dibujo arriba
        {
            vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_puerta_data + i * ANCHO_BLOQUE);
            vgaxfb[aux_index + 1] = (byte)pgm_read_byte_near(img_puerta_data + i * ANCHO_BLOQUE + 1);
        }
        
        aux_index += ANCHO_BLOQUE;//dibujo a la izquierda
        if(x_puerta < 14)
        {
          vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_puerta_data + i * ANCHO_BLOQUE);
          vgaxfb[aux_index + 1] = (byte)pgm_read_byte_near(img_puerta_data + i * ANCHO_BLOQUE + 1);
        }
        
      }
    }
    
    //POSICIONANDO LA PELOTA

    

    //for (i = 0; i < ALTO_BLOQUE; i++)
    {
        //aux_index = (i + y_pelota) * ANCHO * ANCHO_BLOQUE + x_pelota;
        
        switch (rotacion)
        {
        case 0:
        {
          VGAX::blitwmask((byte*)(img_pelota_data_0), (byte*)(img_pelota_mask), ALTO_BLOQUE, ALTO_BLOQUE, x_pelota*4, y_pelota);
           // vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_pelota_data_0 + i * ANCHO_BLOQUE);
            //vgaxfb[aux_index + 1] = (byte)pgm_read_byte_near(img_pelota_data_0 + i * ANCHO_BLOQUE + 1);
        }
        break;
        case 1:
        {
          VGAX::blitwmask((byte*)(img_pelota_data_1), (byte*)(img_pelota_mask), ALTO_BLOQUE, ALTO_BLOQUE, x_pelota*4, y_pelota);
            //vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_pelota_data_1 + i * ANCHO_BLOQUE);
            //vgaxfb[aux_index + 1] = (byte)pgm_read_byte_near(img_pelota_data_1 + i * ANCHO_BLOQUE + 1);
        }
        break;
        case 2:
        {
          VGAX::blitwmask((byte*)(img_pelota_data_2), (byte*)(img_pelota_mask), ALTO_BLOQUE, ALTO_BLOQUE, x_pelota*4, y_pelota);
            //vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_pelota_data_2 + i * ANCHO_BLOQUE);
            //vgaxfb[aux_index + 1] = (byte)pgm_read_byte_near(img_pelota_data_2 + i * ANCHO_BLOQUE + 1);
        }
        break;
        case 3:
        {
          VGAX::blitwmask((byte*)(img_pelota_data_3), (byte*)(img_pelota_mask), ALTO_BLOQUE, ALTO_BLOQUE, x_pelota*4, y_pelota);
            //vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_pelota_data_3 + i * ANCHO_BLOQUE);
            //vgaxfb[aux_index + 1] = (byte)pgm_read_byte_near(img_pelota_data_3 + i * ANCHO_BLOQUE + 1);
        }
        break;
        case 4://la pelota esta muerta
        {
          VGAX::blitwmask((byte*)(img_pelota_muerta_data), (byte*)(img_pelota_muerta_mask),ALTO_BLOQUE, ALTO_BLOQUE, x_pelota*4, y_pelota);
           // vgaxfb[aux_index] = (byte)pgm_read_byte_near(img_pelota_muerta_data + i * ANCHO_BLOQUE);
            //vgaxfb[aux_index + 1] = (byte)pgm_read_byte_near(img_pelota_muerta_data + i * ANCHO_BLOQUE + 1);
        }
        break;
        }
    }

    
}
