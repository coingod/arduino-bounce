#include "keyboard.h"
#include "Arduino.h"
#include "adc.h"

/*
char msgs[5][17] = {
	" Right Key:  OK ",
	" Up Key:     OK ",
	" Down Key:   OK ",
	" Left Key:   OK ",
	" Select Key: OK "};
*/


int key_val_to_tecla[5] = { TECLA_RIGHT, TECLA_UP, TECLA_DOWN, TECLA_LEFT, -1};

int prev_lcd_key_VERT;
int new_lcd_key_VERT;
int prev_lcd_key_HORIZ;
int new_lcd_key_HORIZ;


int NUM_KEYS = 5;
void (*onKeyUp[9])(void);
void (*onKeyDown[9])(void);

// Botones Externos
volatile uint8_t portbhistory = 0xFF;     // default is high because the pull-up
volatile bool key_up_array[9] = {false,false,false,false,false,false,false,false,false};
volatile bool key_down_array[9] = {false,false,false,false,false,false,false,false,false};

ISR (PCINT1_vect)
{
	//hacer el debounce
	uint8_t changedbits;

	changedbits = PINC ^ portbhistory;
	portbhistory = PINC;
	if(changedbits & (1 << PINC2))
	{
		//delay(50);//debounce
		//changedbits = PINC ^ portbhistory;
		//if(changedbits & (1 << PINC2)){
			//portbhistory = PINC;
		if(PINC & (1 << PINC2)){
			//flanco ascendente
			key_down_array[BOTON_A2] = true;
			key_up_array[BOTON_A2] = false;
		}
		else{
			//flanco descendente
			key_up_array[BOTON_A2] = true;
			key_down_array[BOTON_A2] = false;
		}
	//}
	}

	if(changedbits & (1 << PINC3))
	{
		
		
			//PCINT3 changed
		if(PINC & (1 << PINC3)){
			//flanco ascendente
			key_down_array[BOTON_A3] = true;
			key_up_array[BOTON_A3] = false;
		}
		else{
			//flanco descendente
			key_up_array[BOTON_A3] = true;
			key_down_array[BOTON_A3] = false;
		}
		
	}

	if(changedbits & (1 << PINC4))
	{
		if(PINC & (1 << PINC4)){
			//flanco ascendente
			key_down_array[BOTON_A4] = true;
			key_up_array[BOTON_A4] = false;
		}
		else{
			//flanco descendente
			key_up_array[BOTON_A4] = true;
			key_down_array[BOTON_A4] = false;
		}
		
	}

	if(changedbits & (1 << PINC5))
	{
	
		if(PINC & (1 << PINC5)){
			//flanco ascendente
			key_down_array[BOTON_A5] = true;
			key_up_array[BOTON_A5] = false;

		}
		else{
			//flanco descendente
			key_up_array[BOTON_A5] = true;
			key_down_array[BOTON_A5] = false;
		}
		
	}
	
	
	
}

int lastInput;
int lastInput2;


int down = 550;
int up = 400;
int left = 600;
int right = 400;

// Convert ADC value to key number
int get_key_VERT()
{
	
	if(lastInput < up)
		return TECLA_UP;
	else if(lastInput > down)
		return TECLA_DOWN;
	else
		return -1;
}

int get_key_HORIZ()
{
	
	if(lastInput2 < right)
		return TECLA_LEFT;
	else if(lastInput2 > left)
		return TECLA_RIGHT;
	else
		return -1;
}

void kcallbck(int input, int channel){
	if(channel==0)
		lastInput = input;
	if(channel == 1)
		lastInput2 = input;
	
}

struct my_adc_cfg cfg = {0, kcallbck};
struct my_adc_cfg cfg2 = {1, kcallbck};

void keyboard_init() {
	prev_lcd_key_HORIZ = -1;
	new_lcd_key_HORIZ = -1;
	prev_lcd_key_VERT = -1;
	new_lcd_key_VERT = -1;
	adc_init(&cfg);
	adc_add_config(&cfg2);
	DDRC &= ~(1 << DDC2);     // Clear the PC2 pin
	DDRC &= ~(1 << DDC3);     // Clear the PC3 pin
	DDRC &= ~(1 << DDC4);     // Clear the PC4 pin
	DDRC &= ~(1 << DDC5);     // Clear the PC5 pin

	PORTC |= (1 << PORTC2) | (1 << PORTC3) | (1 << PORTC4) | (1 << PORTC5);    // turn On the Pull-up
	// PC2 is now an input with pull-up enabled

	PCICR |= (1 << PCIE1);    // set PCIE1 to enable PCMSK1 scan
	PCMSK1 |= (1 << PCINT10) | (1 << PCINT11) | (1 << PCINT12) | (1 << PCINT13) ;  // set PCINT10 to trigger an interrupt on state change
}

void keyboard_loop() {
	adc_loop();
	
	new_lcd_key_VERT = get_key_VERT();
	int debounce_aux = new_lcd_key_VERT;
	if (prev_lcd_key_VERT != new_lcd_key_VERT)
	{
		delay(25);
		new_lcd_key_VERT = get_key_VERT();
		if (new_lcd_key_VERT == debounce_aux)
		{
			
			if (new_lcd_key_VERT != -1)
			//onKeyDown[new_lcd_key]();
				key_down_array[new_lcd_key_VERT] = true;
			else
				key_up_array[prev_lcd_key_VERT] = true;
			//onKeyUp[prev_lcd_key]();
			prev_lcd_key_VERT = new_lcd_key_VERT;
		}
	}/**/
	
	new_lcd_key_HORIZ = get_key_HORIZ();
	debounce_aux = new_lcd_key_HORIZ;
	
	if (prev_lcd_key_HORIZ != new_lcd_key_HORIZ)
	{
		delay(25);
		new_lcd_key_HORIZ = get_key_HORIZ();
		if (new_lcd_key_HORIZ == debounce_aux)
		{
			if (new_lcd_key_HORIZ != -1)
			//onKeyDown[new_lcd_key]();
				key_down_array[new_lcd_key_HORIZ] = true;
			else
				key_up_array[prev_lcd_key_HORIZ] = true;
			//onKeyUp[prev_lcd_key]();
			prev_lcd_key_HORIZ = new_lcd_key_HORIZ;
		}
	}/**/
	
	//aux_callback(lastInput);
	// LCD Key Pressed
	if(key_down_array[TECLA_UP]){
		delay(25);
		if(key_down_array[TECLA_UP]){
			key_down_array[TECLA_UP] = false;
			onKeyDown[TECLA_UP]();
		}
	}
	if(key_up_array[TECLA_UP]){
		delay(25);
		if(key_up_array[TECLA_UP]){
			key_up_array[TECLA_UP] = false;
			onKeyUp[TECLA_UP]();
		}
	}
	
	if(key_down_array[TECLA_DOWN]){
		delay(25);
		if(key_down_array[TECLA_DOWN]){
			key_down_array[TECLA_DOWN] = false;
			onKeyDown[TECLA_DOWN]();
		}
	}
	if(key_up_array[TECLA_DOWN]){
		delay(25);
		if(key_up_array[TECLA_DOWN]){
			key_up_array[TECLA_DOWN] = false;
			onKeyUp[TECLA_DOWN]();
		}
	}
	
	if(key_down_array[TECLA_LEFT]){
		delay(25);
		if(key_down_array[TECLA_LEFT]){
			key_down_array[TECLA_LEFT] = false;
			onKeyDown[TECLA_LEFT]();
		}
	}
	if(key_up_array[TECLA_LEFT]){
		delay(25);
		if(key_up_array[TECLA_LEFT]){
			key_up_array[TECLA_LEFT] = false;
			onKeyUp[TECLA_LEFT]();
		}
	}
	
	if(key_down_array[TECLA_RIGHT]){
		delay(25);
		if(key_down_array[TECLA_RIGHT]){
			key_down_array[TECLA_RIGHT] = false;
			onKeyDown[TECLA_RIGHT]();
		}
	}
	if(key_up_array[TECLA_RIGHT]){
		delay(25);
		if(key_up_array[TECLA_RIGHT]){
			key_up_array[TECLA_RIGHT] = false;
			onKeyUp[TECLA_RIGHT]();
		}
	}
	
	//botones externos
	if(key_down_array[BOTON_A2]){
		delay(50);
		if(key_down_array[BOTON_A2]){
			key_down_array[BOTON_A2] = false;
			onKeyDown[BOTON_A2]();
		}
	}
	if(key_up_array[BOTON_A2]){
		delay(50);
		if(key_up_array[BOTON_A2]){
			key_up_array[BOTON_A2] = false;
			onKeyUp[BOTON_A2]();
		}
	}
	if(key_down_array[BOTON_A3]){
		delay(50);
		if(key_down_array[BOTON_A3]){
			key_down_array[BOTON_A3] = false;
			onKeyDown[BOTON_A3]();
		}
	}
	if(key_up_array[BOTON_A3]){
		delay(50);
		if(key_up_array[BOTON_A3]){
			key_up_array[BOTON_A3] = false;
			onKeyUp[BOTON_A3]();
		}
	}
	if(key_down_array[BOTON_A4]){
		delay(50);
		if(key_down_array[BOTON_A4]){
			key_down_array[BOTON_A4] = false;
			onKeyDown[BOTON_A4]();
		}
	}
	if(key_up_array[BOTON_A4]){
		delay(50);
		if(key_up_array[BOTON_A4]){
			key_up_array[BOTON_A4] = false;
			onKeyUp[BOTON_A4]();
		}
	}
	if(key_down_array[BOTON_A5]){
		delay(50);
		if(key_down_array[BOTON_A5]){
			key_down_array[BOTON_A5] = false;
			onKeyDown[BOTON_A5]();
		}
	}
	if(key_up_array[BOTON_A5]){
		delay(50);
		if(key_up_array[BOTON_A5]){
			key_up_array[BOTON_A5] = false;
			onKeyUp[BOTON_A5]();
		}
	}	

}

void key_down_callback(void (*handler)(), int tecla) {
	onKeyDown[tecla] = handler;
}

void key_up_callback(void (*handler)(), int tecla) {
	onKeyUp[tecla] = handler;
}